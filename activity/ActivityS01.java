package com.zuitt.activity;

import java.util.Scanner;

public class ActivityS01 {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("First Name:");
        String first = userInput.nextLine();
        System.out.println("Last Name:");
        String last = userInput.nextLine();
        System.out.println("First Subject Grade:");
        double FirstGrade = new Double(userInput.nextLine());
        System.out.println("Second Subject Grade:");
        double SecondGrade = new Double(userInput.nextLine());
        System.out.println("Third Subject Grade:");
        double ThirdGrade = new Double(userInput.nextLine());

        System.out.println("Good day, " + first + " " + last +".");
        System.out.println("Your grade average is: " + (FirstGrade + SecondGrade + ThirdGrade)/3);
    }
}
